// to use: nextflow run compile_stitch.nf -resume
// input parameters
//

params.baseDataDir = '/g/mif/people/gritti/projects/Kerim/gastr-volume-hcr-test-data/data'
params.codeDir = '/g/mif/people/gritti/projects/Kerim/gastr-volume-hcr/code/python'
params.fijiPath = '/home/gritti/Desktop/Fiji.app'
params.condaEnvPath = '/opt/miniconda3/envs/nicola'
params.downsample = 0.1

// derived parameters
params.primaryDir = params.baseDataDir + '/primary'
params.processedDir = params.baseDataDir + '/processed'
params.resultsDir = params.baseDataDir + '/results'

params.xml2csv_script  = params.codeDir + '/_xml2csv.py'
params.compile_script  = params.codeDir + '/_compile_conditions_multifields.py'
params.stitching_script  = params.codeDir + '/_stitch_tiles.py'

primaryExpFolder = params.primaryDir + '/date-*'

nextflow.enable.dsl = 2

// Script : Run pipeline
//

process run_xml2csv {

    publishDir "$params.primaryDir/$x", mode: 'copy', overwrite: true
    maxForks 8

    conda params.condaEnvPath

    input:
        path x

    output:
    //   stdout
        val "$params.primaryDir/$x/metadata_PE.csv"

    // script:
    // """
    // echo "${x}"
    // """
    shell:
        '''
        python !{params.xml2csv_script} --exp_folder !{params.primaryDir}/!{x}
        '''

}

process run_compile {

    publishDir "$params.primaryDir/$x", mode: 'copy', overwrite: true
    maxForks 8

    conda params.condaEnvPath

    input:
        path x
        val y 

    output:
        val "$params.primaryDir/$x/DONE_COMPILING.txt"

    shell:
        """
        python !{params.compile_script} --exp_folder !{params.primaryDir}/!{x} --downsample !{params.downsample}
        """

}

process run_stitching {

    // publishDir "$params.primaryDir/$x", mode: 'copy', overwrite: true
    publishDir "$params.processedDir/$x", mode: 'copy', overwrite: true
    maxForks 8

    conda params.condaEnvPath


    input:
        path x
        val y

    output:
        val "$params.processedDir/$x/DONE_STITCHING.txt"

    shell:
        """
        python !{params.stitching_script} --exp_folder !{params.primaryDir}/!{x} --output_path !{params.processedDir} --downsample !{params.downsample} --fiji_path !{params.fijiPath}
        """

}

workflow {

    primary_exp_folder = Channel.fromPath(primaryExpFolder, type:'dir')
    primary_exp_folder.view()

    meta_PE = run_xml2csv( primary_exp_folder )
    meta_PE.view()
    
    done_compiling = run_compile( primary_exp_folder, meta_PE )
    done_compiling.view()

    done_stitching = run_stitching( primary_exp_folder, done_compiling )
    done_stitching.view()

}
