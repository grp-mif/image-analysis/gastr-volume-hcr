// to use: nextflow run compile_stitch.nf -resume
// input parameters
//

params.baseDataDir = '/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data'
params.codeDir = '/g/mif/people/gritti/projects/Kerim/gastr-volume-hcr/code/python'
params.ilastikPath = '/opt/ilastik-1.4.0-Linux/run_ilastik.sh'
params.condaEnvPath = '/opt/miniconda3/envs/nicola'
params.ilastikProjectName = 'gastr-segmentation.ilp'
params.min_distance = 50
params.opening=10
params.erosion=20
params.min_obj_size=1000
params.min_hole_size=500

// derived parameters
params.primaryDir = params.baseDataDir + '/primary'
params.processedDir = params.baseDataDir + '/processed'
params.resultsDir = params.baseDataDir + '/results'

params.analysis_script  = params.codeDir + '/run_analysis_single_folder.py'

processedExpFolder = params.processedDir + '/date-*'

nextflow.enable.dsl = 2

// Script : Run pipeline
//

process run_analysis {

    publishDir "$params.resultsDir/$x", mode: 'copy', overwrite: true
    maxForks 8

    conda params.condaEnvPath

    input:
        path x

    // script:
    // """
    // echo "${x}"
    // """
    shell:
        '''
        python !{params.analysis_script} --exp_folder !{params.processedDir}/!{x} --output_path !{params.resultsDir} --ilastik_path !{params.ilastikPath} --ilastik_project_name !{params.ilastikProjectName} --min_distance !{params.min_distance} --opening !{params.opening} --erosion !{params.erosion} --min_obj_size !{params.min_obj_size} --min_hole_size !{params.min_hole_size}
        '''

}


workflow {

    exp_folder = Channel.fromPath(processedExpFolder, type:'dir')
    exp_folder.view()

    run_analysis( exp_folder )

}
