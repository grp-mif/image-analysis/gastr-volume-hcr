import os
from _xml2csv import xml2csv
from _extract_channel_order import extract_channel_order
from _compile_conditions_multifields import compile_conditions_multifields
from _stitch_tiles import stitch_all_wells

############################################################
### PARAMETERS
data_path = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data"
fiji_path = "/home/gritti/Desktop/Fiji.app"
exp_name = [
    # "date-20220304_hpa-96_plate-1_exp-1",
    # "date-20220304_hpa-96_plate-1_exp-3",
    # "date-20230213_hpa-96_plate-1_exp-1",
    # "date-20230213_hpa-96_plate-1_exp-2",
    # "date-20230215_hpa-96_plate-1_exp-1",
    # "date-20230216_hpa-96_plate-1_exp-1",
    # "date-20230309_hpa-96_plate-1_exp-1",
    # "date-20230309_hpa-96_plate-1_exp-2",
    # "date-20230309_hpa-96_plate-1_exp-3",
    "date-20230915_hpa-96_plate-1_exp-1",
    "date-20230915_hpa-96_plate-1_exp-2",
    "date-20230915_hpa-96_plate-1_exp-3",
]
downsample = 0.1

##### PARAMETERS FOR TESTS NICOLA
# data_path = "/g/mif/people/gritti/projects/Kerim/gastr-volume-hcr-test-data/data"
# fiji_path = "/home/gritti/Desktop/Fiji.app"
# exp_name = [
#     "date-20220304_hpa-96_plate-1_exp-3",
#     "date-20230213_hpa-96_plate-1_exp-1",
# ]
# downsample = 0.1

############################################################
primary_path = os.path.join(data_path, "primary")
processed_path = os.path.join(data_path, "processed")

exp_folders = [ os.path.join(primary_path, name) for name in exp_name ]

for exp_folder in exp_folders:
    print("-"*25+"\n\t %s"%exp_folder) 
    
    ### block1

    xml2csv(exp_folder,
        image_folder = os.path.join("Images"),
        meta_file_name = "metadata_PE.csv",
        save = True)

    ### block2

    channel_order, df, luts_name, ffs = extract_channel_order(exp_folder)

    print(channel_order)

    compile_conditions_multifields(
        exp_folder, 
        channel_order, 
        luts_name, 
        df,
        ffs,
        ff_mode = "PE", 
        downsample = downsample,
        outfolder = "compiled",
        image_folder = "Images",
        which_proj = 'mean',
        )

    ### block3

    stitch_all_wells(exp_folder, processed_path, downsample, fiji_path)

