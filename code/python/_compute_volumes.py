import os
import numpy as np
import pandas as pd
from skimage import measure
from skimage import filters
from collections.abc import Iterable

def compute_volumes(imgs, labels, meta, 
                    min_perc=10,
                    max_perc=90,
                    control=False,
                    manual=False,
                    thresholds_manual=100,
                    thrs_control=None):

    if not isinstance(min_perc, Iterable):
        min_perc= [min_perc for j in range(meta["n_ch"]-1)]
    if not isinstance(max_perc, Iterable):
        max_perc= [max_perc for j in range(meta["n_ch"]-1)]
    if not isinstance(thresholds_manual, Iterable):
        thresholds_manual= [thresholds_manual for j in range(meta["n_ch"]-1)]
    
    props = pd.DataFrame(measure.regionprops_table(labels, properties=('label','bbox')))
    props = props.astype("int")
    # props.to_csv("props.csv")

    ch_order_names = [meta["ch%d"%i] for i in range(meta["n_ch"])]
    ch_order_names = [ch_name for ch_name in ch_order_names if ch_name != "BF"]

    props_final = pd.concat([pd.DataFrame(meta, index=[i for i in range(len(props))]), props], axis=1)
    props_final["Area"] = 0
    masks = {ch_name: np.zeros(imgs[0].shape) for ch_name in ch_order_names}
    for ch_name in ch_order_names:
        props_final["Area_%s"%ch_name] = 0

    # print(meta)
    # print(props)
    # print(ch_order_names)

    # import matplotlib.pyplot as plt
    # fig, ax = plt.subplots(len(props.label),5)

    thrs = [None for j in ch_order_names]
    # if a control experiment, compute threhsolds
    # otherwise, set the thresholds to the ones computed on the control
    if control:
        print("computing thresholds")
        if not manual:
            for j, ch_name in enumerate(ch_order_names):
                gastr_pxls = imgs[j+1][labels!=0].flatten()
                lims = np.percentile(gastr_pxls, (min_perc[j], max_perc[j]))
                gastr_pxls = np.clip(gastr_pxls, lims[0], lims[1])
                thrs[j] = filters.threshold_otsu(gastr_pxls)
        else:
            thrs = thresholds_manual
    else:
        print("using control thresholds")
        thrs = thrs_control
    print(thrs)

    for i, p in props.iterrows():
        # print(p)
        # print(i,p)
        gastr_img = imgs[:,p['bbox-0']:p['bbox-2'],p['bbox-1']:p['bbox-3']]
        gastr_label_img = labels[p['bbox-0']:p['bbox-2'],p['bbox-1']:p['bbox-3']]
        gastr_label = p.label
        gastr_mask = gastr_label_img>0
    #     ax[i,0].imshow(gastr_img[0])
    #     ax[i,1].imshow(gastr_img[1])
    #     ax[i,2].imshow(gastr_img[2])
    #     ax[i,3].imshow(gastr_img[3])
    #     ax[i,4].imshow(gastr_label)

        props_final.at[i,"Area"] = np.sum(gastr_mask)

        for j, ch_name in enumerate(ch_order_names):
            pxl_vals = gastr_img[j+1][gastr_mask].flatten()
            # print(np.sum(gastr_mask), pxl_vals.shape)
            thr = thrs[j]

            area = np.sum(pxl_vals>thr)
            props_final.at[i, "Area_%s"%ch_name] = area

            mask = (imgs[j+1]>thr) * (labels==gastr_label) * labels
            masks[ch_name] += mask

    # print(props_final)
    # plt.show()

    return props_final, masks, thrs


if __name__=="__main__":
    from _extract_meta import extract_meta
    from _load_tifs import load_pngs
    from _label_gastruloids import label_gastruloids

    exp_folder = os.path.join("..","data","primary","control_20231005_72hpa_plate-1_exp-1")
    well_name = "A01"

    meta = extract_meta(exp_folder, well_name)
    print(meta)
    imgs = load_pngs(exp_folder, well_name, meta)
    print("Image shape:", imgs.shape)
    labels = label_gastruloids(imgs, meta)
    print("Number of objects found:", np.max(labels))
    props_final, masks = compute_volumes(imgs, labels, meta)
    print(props_final)
    print(masks.shape)


