import os
from skimage import filters
from skimage import morphology
from skimage import segmentation
from skimage import measure
from skimage import feature
import numpy as np
from scipy import ndimage as ndi
import matplotlib.pyplot as plt
import subprocess
from skimage.io import imread, imsave

def label_gastruloids(exp_folder, well_name, 
                      ilastik_path,
                      ilastik_proj, 
                      min_distance=10,
                      opening=10,
                      erosion=10,
                      min_obj_size=1000,
                      min_hole_size=1000):

    # use ilastik
    subprocess.run([
        "%s"%ilastik_path,
        "--headless",
        "--project=%s"%ilastik_proj,
        "--raw_data=%s"%os.path.join(exp_folder, well_name, "stitched_BF.tif"),
        "--output_format=tif",
        "--export_source=%s"%("Simple Segmentation"),
        "--output_filename_format=%s"%os.path.join(exp_folder, well_name, "BF_ilastik.tif"),
    ])

    mask = imread(os.path.join(exp_folder, well_name, "BF_ilastik.tif"))
    mask[mask!=1] = 0
    imsave(os.path.join(exp_folder, well_name, "BF_ilastik.tif"), mask.astype(np.uint8), check_contrast=False)

    # massage masks
    mask = morphology.remove_small_objects(mask, min_size=min_obj_size)
    mask = morphology.remove_small_holes(mask, area_threshold=min_hole_size)
    mask = morphology.binary_opening(mask, morphology.disk(opening))

    ### perform watershed
    # erode mask to separate individual gastruloids
    mask_small = morphology.binary_erosion(mask, morphology.disk(erosion))
    mask_small = morphology.binary_opening(mask_small, morphology.disk(int(opening/2)))
    mask_small = morphology.remove_small_objects(mask_small, min_size=int(min_obj_size/2))
    distance_small = ndi.distance_transform_edt(mask_small)
    labels_small = measure.label(mask_small)
    # find maxima in distance transform
    coords = feature.peak_local_max(
        distance_small, 
        min_distance=min_distance, 
        # labels=mask
        labels=labels_small,
        num_peaks_per_label=1,
    )
    markers = np.zeros(distance_small.shape, dtype=bool)
    markers[tuple(coords.T)] = True
    markers, _ = ndi.label(markers)

    # make watershed
    distance = ndi.distance_transform_edt(mask)
    labels = segmentation.watershed(-distance, markers, mask=mask)
    # remove small objects and relable and remove border objects
    labels = morphology.remove_small_objects(labels, min_size=min_obj_size)
    labels = measure.label(labels)
    labels = segmentation.clear_border(labels)
    labels = measure.label(labels)

    return mask, distance_small, labels

if __name__ == "__main__":
    from _extract_meta import extract_meta
    from _load_tifs import load_pngs

    exp_folder = os.path.join("..","data","primary","control_20231005_72hpa_plate-1_exp-1")
    well_name = "A01"

    meta = extract_meta(exp_folder, well_name)
    imgs = load_pngs(exp_folder, well_name, meta)
    labels = label_gastruloids(imgs, meta)

    import matplotlib.pyplot as plt
    fig,ax = plt.subplots(1,len(imgs)+1)
    for i in range(len(imgs)):
        ax[i].imshow(imgs[i])
    ax[-1].imshow(labels)
    plt.show()

    assert np.max(labels) == 16, "Test not passed!"

