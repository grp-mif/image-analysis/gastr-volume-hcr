import os

def extract_meta(exp_folder, well_name="_"):
    exp_infos = exp_folder.split(os.sep)[-1]

    sample_meta = {}

    exp_infos = exp_infos.split("_")
    print(exp_folder, exp_infos)
    # print(exp_infos)
    sample_meta['date'] = exp_infos[0]
    sample_meta['hpa'] = exp_infos[1]
    sample_meta['plate'] = exp_infos[2]
    sample_meta['experiment'] = exp_infos[3]
    sample_meta['well'] = well_name

    with open(os.path.join(exp_folder, "metadata.txt")) as f:
        lines = f.readlines()
    for line in lines:
        key, val = line.strip().split(" = ")
        sample_meta[key] = val
    
    sample_meta['n_ch'] = len([c for c in sample_meta.keys() if "ch" in c])

    return sample_meta

if __name__=="__main__":
    exp_folder = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data/primary/date-20220304_hpa-96_plate-1_exp-1"
    well_name = "A01"

    meta_expected = {
        'condition': 'control', 
        'date': '20231005', 
        'plate': '72hpa', 
        'experiment': 'plate-1', 
        'well': 'A01', 
        'scale_xy': '0.666', 
        'unit_xy': 'um', 
        'ch0': 'BF', 
        'ch1': 'Bra', 
        'ch2': 'Gata6', 
        'ch3': 'Meox', 
        'best': 'Gata6', 
        'n_ch': 4
    }

    meta = extract_meta(exp_folder, well_name)

    print(meta)

    assert meta == meta_expected, "Test not passed!"