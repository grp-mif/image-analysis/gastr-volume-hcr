import os, glob
import numpy as np
from skimage import io

def load_tifs(exp_folder, well_name, meta):

    file_list = glob.glob(os.path.join(exp_folder, well_name,"*.tif"))
    file_list = [f for f in file_list if "merge" not in f]
    # print(file_list)

    imgs = [None for i in range(meta['n_ch'])]
    for i in range(meta['n_ch']):
        ch_name = meta['ch%d'%i]
        file_index = ["stitched_%s.tif"%ch_name == os.path.split(f)[-1] for f in file_list].index(True)
        print(file_index)
        file_name = file_list[file_index]
        print(ch_name, file_name)
        img = io.imread(file_name)
        img[img==0] = np.mean(img[img!=0])
        imgs[i] = img

    imgs = np.stack(imgs)

    return imgs

if __name__=="__main__":
    from _extract_meta import extract_meta
    
    exp_folder = os.path.join("..","data","primary","control_20231005_72hpa_plate-1_exp-1")
    well_name = "A01"

    exp_meta = extract_meta(exp_folder, well_name)
    imgs = load_tifs(exp_folder, well_name, exp_meta)

    print(imgs.shape)
    print(exp_meta)

    assert imgs.shape == (4,1000,1000), "Test not passed!"
