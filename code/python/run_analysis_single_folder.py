import os
from _extract_meta_processed import extract_meta_processed
from _load_tifs import load_tifs
from _label_gastruloids import label_gastruloids
from _compute_volumes import compute_volumes
from _save_results import save_results


def run_analysis_single_folder(exp_folder, output_path, 
                               ilastik_path, ilastik_project_name,
                               min_distance, opening, erosion, 
                               min_obj_size, min_hole_size,
                               min_perc, max_perc,
                               manual, thresholds_manual):

    print("-"*25)
    #######################
    base_dir, exp_name = os.path.split(exp_folder)
    print(base_dir, exp_name,  output_path)
    save_folder = os.path.join(output_path, exp_name)
    if not os.path.exists(save_folder):
        os.mkdir(save_folder)

    ### find all wells inside folder
    well_names = [f.name for f in os.scandir(exp_folder) if f.is_dir()]

    # start from the first well=control
    for i, w in enumerate(well_names):
        if "_control" in w:
            well_names[0], well_names[i] = well_names[i], well_names[0]

    #######################
    ### run analysis on all well_folders

    ilastik_project = os.path.join(exp_folder, ilastik_project_name)

    for i, well_name in enumerate(well_names):
        print("Processing folder:\n%s, %s"%(exp_folder, well_name))

        print("\tExtracting meta...")
        meta = extract_meta_processed(exp_folder, well_name)
        print(meta)
        print("\tLoading tifs...")
        imgs = load_tifs(exp_folder, well_name, meta)
        print("\tImage shape:", imgs.shape)
        print("\tLabeling objects...")
        mask, distance, labels = label_gastruloids(
            exp_folder, 
            well_name, 
            ilastik_path,
            ilastik_project, 
            min_distance=min_distance,
            opening=opening,
            erosion=erosion,
            min_obj_size=min_obj_size,
            min_hole_size=min_hole_size
        )
        # if it's trhe first well (control), then compute thresholds
        # otherwise, use previously computed thresholds
        if not manual:
            if i==0:
                props_final, channel_labels, thrs_control = compute_volumes(imgs, labels, meta, 
                                                                            min_perc=min_perc, max_perc=max_perc, 
                                                                            control=True, thrs_control=None)
            else:
                props_final, channel_labels, _ = compute_volumes(imgs, labels, meta, 
                                                                min_perc=min_perc, max_perc=max_perc, 
                                                                control=False, thrs_control=thrs_control)
        else:
            props_final, channel_labels, _ = compute_volumes(imgs, labels, meta, 
                                                            manual=manual, thresholds_manual=thresholds_manual)

        print("\tNumber of objects found:", len(props_final))
        print("\tSaving results...")
        save_results(save_folder, mask, distance, labels, meta, props_final, channel_labels)
        print("\tDone.")

    return

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_folder")
    parser.add_argument("--output_path")
    parser.add_argument("--ilastik_path")
    parser.add_argument("--ilastik_project_name")
    parser.add_argument("--min_distance", 
                        type=int, default=50, 
                        help='Minimum distance in pixels between gastruloids centroids.')
    parser.add_argument("--opening", 
                        type=int, default=10, 
                        help='Level of opening (smoothing) of gastruloids.')
    parser.add_argument("--erosion", 
                        type=int, default=20, 
                        help='Level of erosion to separate gastruloids.')
    parser.add_argument("--min_obj_size", 
                        type=int, default=1000, 
                        help='Minimum gastruloid size (in pixels). Smaller objects will be discarded.')
    parser.add_argument("--min_hole_size", 
                        type=int, default=500, 
                        help='Minimum hole size. Smaller holes will be filled.')

    args = parser.parse_args()

    exp_folder = args.exp_folder
    output_path = args.output_path
    ilastik_path = args.ilastik_path
    ilastik_project_name = args.ilastik_project_name
    min_distance = args.min_distance
    opening = args.opening
    erosion = args.erosion
    min_obj_size = args.min_obj_size
    min_hole_size = args.min_hole_size

    run_analysis_single_folder(exp_folder, output_path, 
                               ilastik_path, ilastik_project_name,
                               min_distance, opening, erosion, min_obj_size, min_hole_size)
