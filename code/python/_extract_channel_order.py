import os
import pandas as pd
from _extract_meta import extract_meta

def extract_channel_order(path):
    meta = extract_meta(path)
    n_ch = meta["n_ch"]
    df = pd.read_csv(os.path.join(path,"metadata_PE.csv"))
    ch_name_list = list(df.chName.values[:n_ch])
    print(ch_name_list)

    channel_order = []
    if "Brightfield" in ch_name_list:
        channel_order.append(ch_name_list.index("Brightfield"))
    for dye in ["Alexa "+i for i in ["405", "488", "515", "568", "594", "647", "685"]]:
        if dye in ch_name_list:
            channel_order.append(ch_name_list.index(dye))
    # print(channel_order)
    luts_name = ["gray", "green", "orange", "red", "yellow", "maroon", "violet"][:n_ch]
    ffs = [None for i in range(n_ch)]

    return channel_order, df, luts_name, ffs