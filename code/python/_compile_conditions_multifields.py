import numpy as np
from skimage.io import imread
from skimage.transform import rescale
import tifffile
import os, string, tqdm
import pandas as pd
from _make_lut import make_lut
from _imagej_metadata_tags import imagej_metadata_tags
from _extract_channel_order import extract_channel_order
from _extract_ffc_info import extract_ffc_info
from _extract_meta import extract_meta

def compile_conditions_multifields(
        path, 
        channel_order, 
        luts_name, 
        df,
        ffs,
        downsample=0.1,
        ff_mode = "PE", 
        outfolder = "compiled",
        image_folder = os.path.join("Images"),
        which_proj = 'mean',
        ):
        
    # ff_mode: 'PE' for PE FF correction, use 'slide' for autofluorescence slide, use 'None' for no correction
    ffs = [1. for ff in ffs]
    if ff_mode == "slide":
        ffs = [ff/np.median(ff) if ff is not None else 1. for ff in ffs]
    elif ff_mode == "PE":
        ffs_info = extract_ffc_info(path, channel_order)
        ffs = [ff_info["ff_profile"] for ff_info in ffs_info]

    meta = extract_meta(path, "_")

    # find out all wells
    wells = df.groupby(["row","col"]).size().reset_index()

    # define well id to convert e.g. r01c01 into A01
    d = dict(enumerate(string.ascii_uppercase, 1))

    pbar = tqdm.tqdm(wells.iterrows())
    for i, p in pbar:
        r = int(p.row)
        c = int(p.col)
        well = d[r]+"%02d"%c

        # print(well, meta[well])
        
        conversion = pd.DataFrame({})

        pbar.set_description(well)
        pbar.update()
        
        if well not in meta:
            continue
        
        outpath = os.path.join(path, outfolder, "%s_%s"%(well, meta[well]))
        if not os.path.exists(outpath):
            os.makedirs(outpath)
            
        df_well = df[(df.row==r)&(df.col==c)]
        
        # find all fields inside this well
        fields = df_well.groupby(["Ypos","Xpos"]).size().reset_index()
        fields = fields.sort_values(by=["Ypos","Xpos"])
        l = list(set(fields.Ypos))
        l.sort()
        fields["Yidx"] = [l.index(v) for v in fields.Ypos]
        l = list(set(fields.Xpos))
        l.sort()
        fields["Xidx"] = [l.index(v) for v in fields.Xpos]
        
        for j, f in tqdm.tqdm(fields.iterrows(), total = len(fields)):
            x = f.Xpos
            y = f.Ypos
            xidx = f.Xidx
            yidx = f.Yidx
            # print("-"*25,"x:",xidx,"y:",yidx,"j:",j)
            
            df_pos = df_well[(df_well.Xpos==x)&(df_well.Ypos==y)]
            
            # print('-'*50)
            # print(df_pos)
            

            if len(df_pos)>0:

                # print(df_pos.filename.values[0])

                # print('Images found')
                stack = []
                for k, ch in enumerate(channel_order):
                    df_pos_ch = df_pos[df_pos.channel==(ch+1)]
                    df_pos_ch = df_pos_ch.sort_values(by="Zpos")
                    # print("-"*25,"x:",xidx,"y:",yidx,"ch:",ch,"j:",j)
                    # print(df_pos_ch)
                    # [print(img_file) for img_file in df_pos_ch.filename]
                    # print(df_pos_ch.filename.values[0])
                    # print([os.path.join(folder_raw,exp_folder,'Images',img_file) for img_file in df_pos_ch.filename])
                    stack_ch = np.stack([imread(os.path.join(path,image_folder,img_file))//ffs[k] for img_file in df_pos_ch.filename])
                    stack.append(stack_ch.astype(np.uint16))
    
                # order channels
                stacks = np.array(stack).astype(np.uint16)
    
                # create imagej metadata with LUTs
                luts_dict = make_lut(luts_name)
                # luts_dict = make_lut_old()
                ijtags = imagej_metadata_tags({"LUTs": [luts_dict[lut_name] for lut_name in luts_name]}, ">")
                
                outname = "field%03d.tif"%j
    
                raw = pd.DataFrame({"tile_idx":[j],
                                    "filename":[outname],
                                    "row_idx":[yidx],
                                    "col_idx":[xidx],
                                    "example_filename_original":[df_pos.field.values[0]]
                                    })
                conversion = pd.concat([conversion,raw], ignore_index=True)
    
                # print(outname)
                # print(stacks.shape)
                if which_proj=='mip':
                    tosave = []
                    for k, s in enumerate(stacks):
                        if k==0:
                            tosave.append(np.min(s, 0))
                        else:
                            tosave.append(np.max(s, 0))
                    tosave = np.stack(tosave).astype(np.uint16)
                elif which_proj=='mean':
                    tosave = []
                    for k, s in enumerate(stacks):
                        if k==0:
                            tosave.append(np.min(s, 0))
                        else:
                            tosave.append(np.mean(s, 0))
                    tosave = np.stack(tosave).astype(np.uint16)
                else:
                    tosave = np.swapaxes(stacks, 0, 1).astype(np.uint16)
                # print(tosave.shape)
                tosave = rescale(tosave, (1,*[downsample for y in range(tosave.ndim-1)]), order=1, preserve_range=True, anti_aliasing=True)
                tosave = tosave.astype(np.uint16)
                # print(tosave.shape)
                tifffile.imwrite(os.path.join(outpath,outname),tosave, byteorder='>', imagej=True,
                                metadata={"mode": "composite"}, extratags=ijtags, 
                                # check_contrast=False
                                )
                
            else:
                print("COULD NOT FIND ANY IMAGE IN THIS POSITION!!!")
            
        conversion.to_csv(os.path.join(outpath, "metadata.csv"))

    open(os.path.join(path, 'DONE_COMPILING.txt'), 'w').close()


def test_compile():

    path = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data/primary/date-20220304_hpa-96_plate-1_exp-1"

    channel_order, df, luts_name, ffs = extract_channel_order(path)

    compile_conditions_multifields(
        path, 
        channel_order, 
        luts_name, 
        df,
        ffs,
        ff_mode = "PE", 
        downsample = 0.1,
        outfolder = "compiled",
        image_folder = os.path.join("Images"),
        do_mip = True,
        )

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_folder")
    parser.add_argument("--downsample", 
                        type=float, default=0.1, 
                        help='Float to downsample data (default=0.1)')

    args = parser.parse_args()

    exp_folder = args.exp_folder
    downsample = args.downsample

    channel_order, df, luts_name, ffs = extract_channel_order(exp_folder)

    compile_conditions_multifields(
        exp_folder, 
        channel_order, 
        luts_name, 
        df,
        ffs,
        ff_mode = "PE", 
        downsample = downsample,
        outfolder = "compiled",
        image_folder = "Images",
        which_proj = 'mean',
        )