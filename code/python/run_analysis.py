import os
import pandas as pd
from run_analysis_single_folder import run_analysis_single_folder

############################################################
### PARAMETERS
data_path = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data"

exp_name = [
    # "date-20220304_hpa-96_plate-1_exp-9",
    # "date-20230213_hpa-96_plate-1_exp-9",
    # "date-20230216_hpa-96_plate-1_exp-9",
    # "date-20230309_hpa-96_plate-1_exp-9",
    "date-20230915_hpa-96_plate-1_exp-9",
]

min_distance = 50
opening=10
erosion=20
min_obj_size=1000
min_hole_size=500
min_perc=[5,10,10]
max_perc=[95,90,90]
manual=False
thresholds_manual=100

ilastik_path = "/opt/ilastik-1.4.0-Linux/run_ilastik.sh"
ilastik_project_name = "gastr-segmentation.ilp"

##### PARAMETERS FOR TESTS NICOLA
# data_path = '/g/mif/people/gritti/projects/Kerim/gastr-volume-hcr-test-data/data'

# exp_name = [
#     # "date-20220304_hpa-96_plate-1_exp-3",
#     "date-20230213_hpa-96_plate-1_exp-1"
# ]

# min_distance = 50
# opening=10
# erosion=20
# min_obj_size=1000
# min_hole_size=500
# min_perc=10
# max_perc=90

# ilastik_path = "/opt/ilastik-1.4.0-Linux/run_ilastik.sh"
# ilastik_project_name = "gastr-segmentation.ilp"

############################################################

primary_path = os.path.join(data_path, "primary")
processed_path = os.path.join(data_path, "processed")
results_path = os.path.join(data_path, "results")

exp_folders = [os.path.join(processed_path, name) for name in exp_name]

for exp_folder in exp_folders:
    run_analysis_single_folder(exp_folder, results_path, 
                                ilastik_path, ilastik_project_name,
                                min_distance, opening, erosion, 
                                min_obj_size, min_hole_size,
                                min_perc, max_perc,
                                manual, thresholds_manual
    )
