import os
import pandas as pd

def extract_meta_processed(exp_folder, well_name):
    meta = pd.read_csv(os.path.join(exp_folder, well_name, "metadata.csv"), header=None, index_col=0).squeeze("columns")
    meta = dict(meta)

    meta["n_ch"] = int(meta["n_ch"])
    meta["scale_xy"] = float(meta["scale_xy"])

    return meta
