import os, glob
import numpy as np
import pandas as pd
import tifffile
from _make_lut import make_lut
from _imagej_metadata_tags import imagej_metadata_tags

def save_results(save_folder, mask, distance, labels, meta, props_final, masks):

    if not os.path.exists(save_folder):
        os.mkdir(save_folder)

    mask_image_name = os.path.join(save_folder, meta["well"]+"_masks.tif")
    tifffile.imwrite(mask_image_name, mask.astype(np.uint16))

    distance_image_name = os.path.join(save_folder, meta["well"]+"_distance.tif")
    tifffile.imwrite(distance_image_name, distance.astype(np.uint16))

    luts_name = ["green"]
    luts_dict = make_lut(luts_name)
    ijtags = imagej_metadata_tags({"LUTs": [luts_dict[lut_name] for lut_name in luts_name]}, ">")
    print(luts_dict["green"].shape)

    label_image_name = os.path.join(save_folder, meta["well"]+"_labels.tif")
    tifffile.imwrite(
        label_image_name, labels.astype(np.uint16),
            imagej=True, 
            metadata={"mode": "color"}, 
            extratags=ijtags
    )

    for ch_name in masks:
        mask_image_name = os.path.join(save_folder, meta["well"]+"_labels_%s.tif"%ch_name)
        tifffile.imwrite(
            mask_image_name, masks[ch_name].astype(np.uint16),
            imagej=True, 
            metadata={"mode": "color"}, 
            extratags=ijtags
        )

    df_name = os.path.join(save_folder, meta["well"]+"_props.csv")
    props_final.to_csv(df_name)

    return

if __name__ == "__main__":

    from _extract_meta import extract_meta
    from _load_tifs import load_pngs
    from _label_gastruloids import label_gastruloids
    from _compute_volumes import compute_volumes

    exp_folder = os.path.join("..","data","primary","control_20231005_72hpa_plate-1_exp-1")
    well_name = "A01"
    output_dir = os.path.join("..","data","results")

    meta = extract_meta(exp_folder, well_name)
    print(meta)
    imgs = load_pngs(exp_folder, well_name, meta)
    print("Image shape:", imgs.shape)
    labels = label_gastruloids(imgs, meta)
    print("Number of objects found:", np.max(labels))
    props_final, channel_labels = compute_volumes(imgs, labels, meta)
    print(props_final)
    save_results(output_dir, labels, meta, props_final, channel_labels)

