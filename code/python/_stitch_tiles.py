import os, glob
import pandas as pd
from _extract_channel_order import extract_channel_order
from _extract_meta import extract_meta
import numpy as np
import imagej
    
def stitch_single_well(path, well_name, fiji_path):

    tile_layout = pd.read_csv(os.path.join(path,"compiled",well_name,"metadata.csv"))
    n_rows = int(np.max(tile_layout.row_idx)+1)
    n_cols = int(np.max(tile_layout.col_idx)+1)


    ij = imagej.init(fiji_path)
    # print(ij.getVersion())
    # print("%s--------%d-----------%d"%(well_name,n_cols,n_rows))

    plugin = "Grid/Collection stitching"
    args = {
        "type": "[Grid: row-by-row]",
        "order": "[Right & Up]",
        "grid_size_x": "%d"%n_cols,
        "grid_size_y": "%d"%n_rows,
        "tile_overlap": "10",
        "first_file_index_i": "0",
        "directory": os.path.join(path, "compiled", well_name),
        "file_names": "field{iii}.tif",
        "output_textfile_name": "TileConfiguration.txt",
        "fusion_method": "[Linear Blending]",
        "regression_threshold": "0.30",
        "max/avg_displacement_threshold": ".1",
        "absolute_displacement_threshold": ".1",
        "compute_overlap": True,
        "subpixel_accuracy": True,
        "computation_parameters": "[Save computation time (but use more RAM)]",
        "image_output": "[Write to disk]",
        "output_directory": "%s"%os.path.join(path,"compiled",well_name)
    }
    ij.py.run_plugin(plugin, args)
    ij.getContext().dispose()

def move_single_well(path, well_name, output_folder, downscale):

    base_dir, exp_name = os.path.split(path)
    channel_order, df, luts_name, ffs = extract_channel_order(path)
    meta = extract_meta(path, "_")

    img_files = glob.glob(os.path.join(path,"compiled",well_name,"img_*"))
    img_files.sort()
    # print(meta)
    # print(channel_order)
    # print(img_files)
    channel_names = [meta["ch%d"%(c)] for c in channel_order]
    # print(channel_names)
    file_names = ["stitched_%s.tif"%c for c in channel_names]

    new_meta = {}
    new_meta["date"] = meta["date"]
    new_meta["hpa"] = meta["hpa"]
    new_meta["plate"] = meta["plate"]
    new_meta["experiment"] = meta["experiment"]
    new_meta["scale_xy"] = float(meta["scale_xy"])/downscale
    new_meta["unit_xy"] = "um"
    for i, c in enumerate(channel_names):
        new_meta["ch%s"%i] = c
    new_meta["n_ch"] = meta["n_ch"]
    new_meta["well"] = well_name.split("_")[0]
    new_meta["condition"] = well_name.split("_")[1]
    new_meta = pd.Series(new_meta)

    output_path = os.path.join(output_folder, exp_name, well_name)
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for f_old, f_new in zip(img_files, file_names):
        file_path = os.path.join(output_path, f_new)
        print(file_path)
        os.rename(f_old, file_path)

    new_meta.to_csv(os.path.join(output_path, "metadata.csv"), header=None)

    # print(new_meta)

def stitch_all_wells(path, output_folder, downscale, fiji_path):
    base_dir, exp_name = os.path.split(path)

    meta = extract_meta(path, "_")
    # print(meta)

    wells_compiled = [f.name for f in os.scandir(os.path.join(path,"compiled")) if f.is_dir()]
    wells_compiled = [w for w in wells_compiled if os.path.exists(os.path.join(path,"compiled",w,"metadata.csv"))]
    # print(wells_compiled)

    wells_todo = [w for w in wells_compiled if w.split("_")[0] in meta]

    # print(wells_todo)

    for well_name in wells_todo:
        # import time
        # print("-"*25)
        # print(well_name)
        # time.sleep(5)
        stitch_single_well(path, well_name, fiji_path)
        move_single_well(path, well_name, output_folder, downscale)

    open(os.path.join(output_folder, exp_name,'DONE_STITCHING.txt'), 'w').close()


def test_stitch_all_wells():
    path = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data/primary/date-20220304_hpa-96_plate-1_exp-1"
    output_folder = "/g/trivedi/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data/processed"
    downscale = 0.1

    stitch_all_wells(path, output_folder, downscale)

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_folder")
    parser.add_argument("--output_path")
    parser.add_argument("--fiji_path")
    parser.add_argument("--downsample", 
                        type=float, default=0.1, 
                        help='Float to downsample data (default=0.1)')

    args = parser.parse_args()

    exp_folder = args.exp_folder
    output_path = args.output_path
    downsample = args.downsample
    fiji_path = args.fiji_path

    stitch_all_wells(exp_folder, output_path, downsample, fiji_path)

