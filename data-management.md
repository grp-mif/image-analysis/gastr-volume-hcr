# Data management

### Data locations

The data are in the below folder which are shared via the DMA with all project members.

`//trivedi.embl.es/Kristina_Stapornwongkul/ImageAnalysis/gastr_hcr_volumes/data`

This contains the following subfolders:

- `data/primary`: primary data acquired by the microscope 
- `data/processed`: processed data
- `data/results`: measurement results

#### Different conditions

The experiments produced under different conditions are stored in:

`data/primary/date-${DATE}_hpa-${HOURS}_${PLATE-ID}_${EXP-ID}/Images/*.tif`

where
- `DATE`: `YYYYMMDD`.
- `HOUR`: hours post aggregation.
- `PLATE-ID`: plate identifier, e.g. `PLATE-1` and `PLATE-2` if two plates were imaged on the same day.
- `EXP-ID`: experiment identifier, e.g. `EXP-1` and `EXP-2` if the same plate was imaged 2 times to image different wells.


### Metadata files

Each experiment folder, contains a `metadata.txt`, which contains:

```
scale_xy = 0.3
unit_xy = um
ch0 = Bra
ch1 = BF
ch2 = Sox17
ch3 = Sox2
A01 = control
A02 = condition2
```

#### Processed and results data

Processed (non-primary) data is located in `data/processed` and `data/results`.

The folder structure is the same as in the `primary` data.

The file names are:

`${SAMPLE-ID}--*.*`

where

`${SAMPLE-ID} = date-${DATE}_hpa-${HOURS}_${PLATE-ID}_${EXP-ID}/${CONDITION}_${W-ID}_labels.tif`

`${SAMPLE-ID} = date-${DATE}_hpa-${HOURS}_${PLATE-ID}_${EXP-ID}/${CONDITION}_${W-ID}_props.csv`



