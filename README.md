# gastruloids HCR analysis

## Description
In this project we aim at analyzing the relative size of different germ layers inside gastruloids under different (metabolic?) conditions.

## Assay
Gastruloids are grown in media supplemented with different ingredients (Control, 2DG, 2DG+CHI, 2DG+Act), collected and stained at XXXhours post aggregation using HCR. 

Typically, the genes of interest are Bra, Sox2 and Sox17.

Subsequently, gastruloids are plated in 96 well plates.

## Data acquisition
Images are acquired for all channels in 3D volumes on the Opera PE system.

In each well, there might be multiple gastruloids, for this reason images in each well are individual FOVs (tiles).

## Data management

Please refer to the [Data Management document](https://git.embl.de/grp-mif/image-analysis/gastr-volume-hcr/-/blob/main/data-management.md?ref_type=heads).

## Usage of this repository

1. Download the repository using the download button at the top right:
![download_button](documentation/download_button.PNG)
1. Follow the [Workflow document](https://git.embl.de/grp-mif/image-analysis/gastr-volume-hcr/-/blob/main/workflow.md) for a detailed description on how to run the code.
